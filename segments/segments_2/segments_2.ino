const int numberPins[] = { 7, 6, 8, 5, 4, 3, 2 };
const int dotPin = 9;

void setup() {
  for(int i = 0; i < 7; i++)
    pinMode(numberPins[i], OUTPUT);
  pinMode(dotPin, OUTPUT);
  pinMode(13, OUTPUT);
  turnOff();
}

void displayDigit(int digit)
{
  static byte numbers[] = { 1, 79, 18, 6, 76, 36, 32, 15, 0, 4 };
  byte mask = 1;
  for(int pin = 0; pin < 7; pin++)
  {
    if(numbers[digit] & (1 << pin))
      digitalWrite(numberPins[pin], HIGH);
    else
      digitalWrite(numberPins[pin], LOW);
    mask = mask * 2;
  }
}

void displayDot()
{
    digitalWrite(dotPin, LOW);
}

void turnOff()
{
  for(int i = 0; i < 7; i++)
    digitalWrite(numberPins[i], HIGH);
  digitalWrite(dotPin, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
 for(int i=0;i<10;i++)
 {
   displayDigit(i);
   displayDot();
   
   delay(1000);
   
   turnOff();
 }

 digitalWrite(13, LOW);
 turnOff();
 delay(500);

  /*
 digitalWrite(13, HIGH);
 
 for (int i = 0; i < 7; i++)
 {
    digitalWrite(numberPins[i], LOW);
    delay(750);
    digitalWrite(numberPins[i], HIGH);
 }
 

 */
}
