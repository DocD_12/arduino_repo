//Цветной графический дисплей 2.2 TFT 320x240     http://iarduino.ru/shop/Displei/cvetnoy-graficheskiy-tft-ekran-320-240---2-2.html

#include <UTFT.h>                              // подключаем библиотеку UTFT
UTFT myGLCD(TFT01_22SP,9,8,12,11,10);             // объявляем объект myGLCD класса библиотеки UTFT  указывая тип дисплея TFT01_22SP и номера выводов Arduino к которым подключён дисплей: SDI/MOSI, SCK, CS, RESET, DC/RS. Можно использовать любые выводы Arduino.
extern uint8_t SmallFont[];                    // подключаем маленький шрифт
extern uint8_t BigFont[];                      // подключаем большой шрифт
extern uint8_t SevenSegNumFont[];              // подключаем шрифт имитирующий семисегментный индикатор
                                               //
void setup(){                                  //
     myGLCD.InitLCD();                           // инициируем дисплей
     myGLCD.setColor(VGA_WHITE);                 // устанавливаем белый цвет текста
     Serial.begin(9600);
}                                              //
                                               //
void loop(){               
     myGLCD.clrScr();                            // стираем всю информацию с дисплея

     
     int sensor = analogRead(A0); 
     String output =  String(sensor);
     Serial.println(sensor);                                            //
     myGLCD.setFont(SevenSegNumFont);            // устанавливаем шрифт имитирующий семисегментный индикатор
     myGLCD.print(output, CENTER, 100);    // выводим текст на дисплей (выравнивание по ширине - центр дисплея, координата по высоте 150 точек)
                                                 //
     delay(500);                               // ждём 20 секунд
}                                              //
