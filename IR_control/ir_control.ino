#include <IRremote.h>
#include <NewTone.h>

int RECEIVE_PIN = 2;
int buz = 3;
int lig = 12;
IRrecv irrecv(RECEIVE_PIN);
decode_results results;

bool light = false;
int tones[] = {261, 293, 329, 349, 392, 440, 493 };
int index = 0;

void setup() {
    Serial.begin(9600);
    irrecv.enableIRIn(); // Start the receiver
    pinMode(buz, OUTPUT);
    pinMode(lig, OUTPUT);
    //digitalWrite(buz, HIGH);
}

void loop() {
   if (irrecv.decode(&results)) {
        //NewTone(buz, 220, 500);
        switch (results.value) {
            case 0xFFA25D:
            case 0x71F9F1D7:
            Serial.println("ONOFF");
            switch_light(); 
            break;
            case 0xFF02FD:
            case 0xEC9A30D9:
            Serial.println("+++");
            index++;
            if (index > 6) index = 0;
            Serial.println(index);
            NewTone(buz, tones[index], 250);
            break;
            case 0xFF9867:
            case 0x99247EBA:
            Serial.println("---");
            index--;
            if (index < 0) index = 6;
            Serial.println(index);
            NewTone(buz, tones[index], 250);
            break;
            case 0xFF22DD:
            case 0xA5B704C1:
            Serial.println("TEST");            
            Serial.println(tones[index]);
            NewTone(buz, tones[index], 1000);
            break;
        }
        irrecv.resume();
    }
}

void beep(){
    NewTone(buz, 220, 1000);
}

void switch_light(){
    light = !light;
    switch (light) {
      case true:
      digitalWrite(lig, HIGH);
      NewTone(buz, 440, 750);
      break;
      case false:
      NewTone(buz, 220, 750);
      digitalWrite(lig, LOW);      
      break;     
    }
}
