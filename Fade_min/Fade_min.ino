int led = 11;           // Пин, к которому подключён затвор транзистора
int brightness = 0;    // Теперь эта переменная отвечает за скорость вращения
int steprise = 25;    // Шаг изменения скорости
int course = -1;
int numStep = 0;

const int numberPins[] = { 7, 6, 8, 5, 4, 3, 2 };
const int dotPin = 9;

void setup()
{ 
  // Настраиваем цифровой пин 9 на вывод
  pinMode(led, OUTPUT);

  for(int i = 0; i < 7; i++)
    pinMode(numberPins[i], OUTPUT);
  pinMode(dotPin, OUTPUT);
  pinMode(13, OUTPUT);
  turnOff(); 
} 

void loop()
{ 
  // Устанавливаем скорость вращения мотора

  for (int i = 0; i<10; i++)
  {
    displayDigit(i);
    brightness = steprise * i;
    analogWrite(led, brightness);   
    delay(500); 
  }
  
  delay(500);    
  turnOff();

  for (int i = 9; i>=0; i--)
  {
    displayDigit(i);
    brightness = steprise * i;
    analogWrite(led, brightness);    
    delay(500);
  }
  
  delay(500);    
  turnOff();
       
}
