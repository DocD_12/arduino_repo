#include <OLED_I2C.h>                          // Подключаем библиотеку OLED_I2C
OLED  myOLED(SDA, SCL, 8);                     // Выбор порта, UNO это SDA 8 pin, SCL - 9 pin.
extern uint8_t SmallFont[];                    // Подключаем шрифт 
 
void setup()
{
  myOLED.begin();                              // инициализация экрана
  myOLED.setFont(SmallFontRus);
}
 
void loop()
{
  myOLED.clrScr();                              // Очищаем экран
  myOLED.print("Ульяночка", CENTER, 24);           // Выводим текст: в центре, строка 24
  myOLED.print("World! =3", CENTER, 40); // Выводим текст: в центре, строка 40
  myOLED.update();                              //
  delay (500);                                  // пауза 0.5 с 
}
