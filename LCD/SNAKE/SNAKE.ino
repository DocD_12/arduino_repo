#include <iarduino_OLED.h>

iarduino_OLED myOLED(0x3C);                            // Объявляем объект myOLED, указывая адрес дисплея на шине I2C: 0x3C или 0x3D.
                                                           //
extern uint8_t SmallFontRus[];                      // Подключаем шрифт 


// set pin numbers for switch, joystick axes, and LED:

const int SW_pin = 12;    // input pin for the mouse pushButton
const int xAxis = A0;         // joystick X axis
const int yAxis = A1;         // joystick Y axis

// parameters for reading the joystick:
int range = 12;               // output range of X or Y movement
int responseDelay = 250;        // response delay of the mouse, in ms
int threshold = range / 4;    // resting threshold
int center = range / 2;       // resting position value

int x_point = 30;
int y_point = 30;
 
void setup()
{
    myOLED.begin();                                        // Инициируем работу с дисплеем.
    myOLED.autoUpdate(false);
    pinMode(SW_pin, INPUT);
    digitalWrite(SW_pin, HIGH);
    Serial.begin(9600);
}
 
void loop()
{
          

    // read and scale the two axes:
    int xReading = readAxis(xAxis);
    int yReading = readAxis(yAxis);

    Serial.println(xReading);
    Serial.println(yReading);
    if (!(digitalRead(SW_pin)))
      Serial.println("PUSHED BUTTON");
    
    x_point += xReading;
    y_point += yReading;
     
    myOLED.drawPixel  (x_point, y_point,                 1);     
    myOLED.update();  

    delay(responseDelay);

    myOLED.clrScr();                                       // Чистим экран.
    myOLED.update();  
}

int readAxis(int thisAxis) {
  // read the analog input:
  int reading = analogRead(thisAxis);

  // map the reading from the analog input range to the output range:
  reading = map(reading, 0, 1023, 0, range);

  // if the output reading is outside from the rest position threshold, use it:
  int distance = reading - center;

  if (abs(distance) < threshold) {
    distance = 0;
  }

  // return the distance for this axis:
  return distance;
}
