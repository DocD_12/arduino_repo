#include <iarduino_OLED_txt.h>

iarduino_OLED_txt myOLED(0x3C);                            // Объявляем объект myOLED, указывая адрес дисплея на шине I2C: 0x3C или 0x3D.
                                                           //
extern uint8_t SmallFontRus[];                      // Подключаем шрифт 
 
void setup()
{
    myOLED.begin();                                        // Инициируем работу с дисплеем.
    myOLED.setFont(SmallFontRus);  
}
 
void loop()
{
    myOLED.clrScr();                                       // Чистим экран.
    //myOLED.print("UTF8", 0, 0);                            // Выводим текст начиная с 0 столбца 0 строки.
    //myOLED.setCoding(TXT_UTF8);                            // Меняем кодировку на UTF-8 (по умолчанию).
    myOLED.print("Ульяночка", OLED_C, 3);  
    myOLED.print("ЦУП", OLED_C, 5); 
    for (int i=0; i<10; i++)
    { 
        myOLED.print("=*", OLED_R, 7); 
        delay (500);  
        myOLED.print("  ", OLED_R, 7); 
        delay (250);  
    }
}
