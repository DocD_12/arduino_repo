// Тестировалось на Arduino IDE 1.0.5
int IN3 = 9; // Input3 подключен к выводу 5
int IN4 = 8;
int led = 13;
int ENB = 10;

void setup()
{
  pinMode (IN4, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (led, OUTPUT);
  pinMode (ENB, OUTPUT); 
  Serial.begin(115200);
}

void loop()
{
  Serial.println ("FIRST Step   4 1  3 0");
  // На пару выводов "IN" поданы разноименные сигналы, мотор вращается
  digitalWrite (IN4, HIGH);
  digitalWrite (IN3, LOW);
  delay(4000);

  Serial.println ("SECOND Step    4 0  3 0");
  // На пару выводов "IN" поданы одноименные сигналы, мотор не вращается
  digitalWrite (IN4, LOW);
  delay(500);

  Serial.println ("THIRD Step    4 0  3 1");
  // На пару выводов "IN" поданы разноименные (и уже противоположные относительно первого случая) сигналы, мотор вращается
  // относительно первого случая) сигналы, мотор вращается в другую сторону
  digitalWrite (IN3, HIGH);
  delay(4000);

  Serial.println ("FOURTH Step    4 0  3 0");
  // Снова на выводах "IN" появились одноименные сигналы, мотор не вращается
  digitalWrite (IN3, LOW);
  delay(5000);
}
