//qweqweqwewqe
int IN3 = 9;
int IN4 = 8;
int led = 13;
int ENB = 10;

void setup()
{
  pinMode (IN4, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (led, OUTPUT);
  pinMode (ENB, OUTPUT); 
  Serial.begin(115200);
}

void loop()
{
  // На пару выводов "IN" поданы разноименные сигналы, мотор готов к вращаению
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
  // подаем на вывод ENB управляющий ШИМ сигнал 
  analogWrite(ENB,55);
  delay(2000);
  analogWrite(ENB,105);
  delay(2000);
  analogWrite(ENB,255);
  delay(2000);
  // Останавливаем мотор повад на вывод ENB сигнал низкого уровеня. 
  // Состояние выводов "IN" роли не играет.
  analogWrite(ENB,0);
  delay(5000);
}