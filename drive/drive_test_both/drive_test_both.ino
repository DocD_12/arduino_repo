//qweqweqwewqe
int IN1 = 7;
int IN2 = 6;
int ENA = 5;

int IN3 = 9;
int IN4 = 8;
int ENB = 10;

void frontOff()
{
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}

void turnLeft()
{
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENA, 125);
  delay(1000);
  frontOff();
}
void turnRight()
{
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENA, 125);
  delay(1000);
  frontOff();
}

void setup()
{
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (ENB, OUTPUT); 
  Serial.begin(115200);
}

void loop()
{
  // На пару выводов "IN" поданы разноименные сигналы, мотор готов к вращаению
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
  // подаем на вывод ENB управляющий ШИМ сигнал 
  analogWrite(ENB,55);
  delay(2000);
  analogWrite(ENB,105);
  delay(2000);
  analogWrite(ENB,255);
  delay(2000);
  // Останавливаем мотор повад на вывод ENB сигнал низкого уровеня. 
  // Состояние выводов "IN" роли не играет.
  analogWrite(ENB,0);
  
  turnLeft();
  turnRight();

  delay(5000);
}