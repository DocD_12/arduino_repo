//qweqweqwewqe
int IN1 = 7;
int IN2 = 6;
int ENA = 5;

int IN3 = 9;
int IN4 = 8;
int ENB = 10;

void frontOff()
{
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}

void gearStop()
{
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
}

void gearForward()
{
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
}

void gearBackward()
{
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
}

void turnLeft()
{
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  analogWrite(ENA, 200);
  delay(1000);
  frontOff();
}
void turnRight()
{
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  analogWrite(ENA, 200);
  delay(1000);
  frontOff();
}

void setup()
{
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (ENB, OUTPUT); 
  Serial.begin(115200);
}

void loop()
{
  gearForward();
  analogWrite(ENB,200);
  //delay(2000);
  
  turnLeft();
  //delay(500);
  turnRight();
  //delay(500);

  gearStop();

  delay(10000);
}