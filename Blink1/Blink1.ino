int led = 13;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
  Serial.begin(115200);
}

// the loop function runs over and over again forever
void loop() {
  Serial.println ("FIRST Step   4 1  3 0");
  //digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second

  for (int i = 0; i < 10; i++)
  {
    digitalWrite(led, HIGH);
    delay(50);
    Serial.print ("!");
    digitalWrite(led, LOW);
    delay(50);       
  }
}
